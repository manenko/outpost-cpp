//
// outpost/src/outpost.cxx
//
// Created by Oleksandr Manenko on 10-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <conker/conker.hxx>
#include <GLFW/glfw3.h>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <random>
#include <set>
#include <vector>

using conker::gfx2d::check_opengl_error;
using conker::gfx2d::clear;
using conker::gfx2d::draw_hline;
using conker::gfx2d::draw_pixel;
using conker::gfx2d::draw_rectangle;
using conker::gfx2d::draw_vline;
using conker::gfx2d::locked_pixels;
using conker::gfx2d::primary_surface;
using conker::gfx2d::rgb_565;
using conker::gfx2d::rgba_8888;
using conker::gfx2d::rgbx_8888;
using conker::gfx2d::setup_2d_camera;
using conker::gfx2d::video_surface;

template <typename Colour>
constexpr Colour
max_colour() noexcept;

template <typename Colour>
Colour
random_colour();

//--------------- max_colour ---------------------------------------------------

template <>
constexpr rgb_565
max_colour() noexcept
{
	return rgb_565(255, 255, 255);
}

template <>
constexpr rgba_8888
max_colour() noexcept
{
	return rgba_8888(255, 255, 255, 255);
}

//--------------- random_colour ------------------------------------------------

template <>
rgb_565
random_colour()
{
	return rgb_565(rand() % 256, rand() % 256, rand() % 256);
}

template <>
rgba_8888
random_colour()
{
	return rgba_8888(rand() % 256, rand() % 256, rand() % 256, 255);
}

template <typename Colour>
void
fill_with_random_colour(locked_pixels<Colour> &li)
{
	clear(li, random_colour<Colour>());
}

template <typename Colour>
void
fill_with_random_pixels(locked_pixels<Colour> &li)
{
	for (auto y = 0; y < li.height; ++y) {
		for (auto x = 0; x < li.width; ++x) {
			li.ptr[x + y * li.width] = random_colour<Colour>();
		}
	}
}

template <typename Colour>
void
animate(locked_pixels<Colour> &li)
{
	static Colour colour;
	auto          ptr = li.ptr;

	for (int i = 0; i < li.height; ++i) {
		for (int j = 0; j < li.width; ++j) {
			*ptr = colour;
			++ptr;
		}

		colour = colour + 32;
	}

	colour = colour + 12;
}

void
get_framebuffer_size(
	GLFWwindow * window,
	std::size_t &width,
	std::size_t &height)
{
	int sw = 0;
	int sh = 0;

	glfwGetFramebufferSize(window, &sw, &sh);

	width  = sw;
	height = sh;
}

void
print_available_video_modes()
{
	auto count = 0;
	auto modes = glfwGetVideoModes(glfwGetPrimaryMonitor(), &count);
	for (auto i = 0; i < count; ++i) {
		std::cout << "Available video modes:" << std::endl
			  << modes->width << "x" << modes->height << "[R"
			  << modes->redBits << ":G" << modes->greenBits << ":B"
			  << modes->blueBits << "]@" << modes->refreshRate
			  << std::endl;
		++modes;
	}
}

int
main()
{
	if (!glfwInit()) {
		return EXIT_FAILURE;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

	print_available_video_modes();

	// auto mode   = glfwGetVideoMode(glfwGetPrimaryMonitor());
	// auto window = glfwCreateWindow(mode->width, mode->height, "Outpost",
	// glfwGetPrimaryMonitor(), nullptr);
	auto window = glfwCreateWindow(800, 600, "Outpost", nullptr, nullptr);

	if (!window) {
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwMakeContextCurrent(window);
	gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress));
	{
		glEnable(GL_TEXTURE_2D);

		std::size_t framebuffer_width  = 0; // mode->width;
		std::size_t framebuffer_height = 0; // mode->height;
		get_framebuffer_size(
			window, framebuffer_width, framebuffer_height);

		primary_surface<rgb_565> ps{framebuffer_width,
					    framebuffer_height};

		while (!glfwWindowShouldClose(window)) {
			setup_2d_camera(framebuffer_width, framebuffer_height);

			glClearColor(1.0, 0.0, 0.0, 1.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
				GL_STENCIL_BUFFER_BIT);

			auto li = ps.lock();
			clear(li, rgb_565(0, 0, 0));

			// li.pixels() // Iterator
			// li.width()
			// li.height()
			// li.begin(), li.end()
			// li[x, y]
			// li[i]
			//
			// Should unlock in destructor?
			// surface.pixels() <- locks ?
			// surface.lock_pixels() <- locks. explicit name?

			// fill_with_random_colour(li);
			fill_with_random_pixels(li);
			// animate(li);

			// auto step       = 1;
			// auto step_count = std::min(framebuffer_height,
			// framebuffer_width) / 2;// - 300; for (auto i = 0; i <
			// step_count; i += step) {
			//     draw_rectangle(li, i, i, framebuffer_width - i*2,
			//     framebuffer_height - i*2,
			//     random_colour<rgb_565>());
			// }

			ps.unlock();

			ps.swap();

			glfwSwapBuffers(window);
			glfwPollEvents();
		}
	}

	glfwTerminate();

	return EXIT_SUCCESS;
}
