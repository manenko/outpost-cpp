#pragma once

//
// conker/include/conker/gfx2d.hxx
//
// Created by Oleksandr Manenko on 18-06-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <conker/gfx2d/colour.hxx>
#include <conker/gfx2d/drawing.hxx>
#include <conker/gfx2d/opengl.hxx>
#include <conker/gfx2d/primary_surface.hxx>
#include <conker/gfx2d/video_surface.hxx>

/*
//\                             /
// \                           /
//  \                         /
//   \                       /
//    ]                     [    ,'|
//    ]                     [   /  |
//    ]___               ___[ ,'   |
//    ]  ]\             /[  [ |:   |
//    ]  ] \           / [  [ |:   |
//    ]  ]  ]         [  [  [ |:   |
//    ]  ]  ]__     __[  [  [ |:   |
//    ]  ]  ] ]\ _ /[ [  [  [ |:   |
//    ]  ]  ] ] (#) [ [  [  [ :===='
//    ]  ]  ]_].nHn.[_[  [  [
//    ]  ]  ]  HHHHH. [  [  [
//    ]  ] /   `HH("N  \ [  [
//    ]__]/     HHH  "  \[__[
//    ]         NNN         [
//    ]         N/"         [
//    ]         N H         [
//   /          N            \
//  /           q,            \
// /                           \
///                             \
*/
