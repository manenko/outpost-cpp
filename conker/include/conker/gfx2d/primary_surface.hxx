//
// conker/include/conker/gfx2d/primary_surface.hxx
//
// Created by Oleksandr Manenko on 16-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#pragma once

#include <conker/gfx2d/colour.hxx>
#include <conker/gfx2d/opengl.hxx>
#include <conker/gfx2d/video_surface.hxx>
#include <cstddef>

namespace conker::gfx2d {
namespace internal {
template <typename Colour>
constexpr GLenum
texel_internal_format() noexcept;

template <typename Colour>
constexpr GLenum
texel_format() noexcept;

template <typename Colour>
constexpr GLenum
texel_type() noexcept;

// rgb_565 -------------------------------------------------------------

template <>
constexpr GLenum
texel_internal_format<rgb_565>() noexcept
{
	return GL_RGB;
}

template <>
constexpr GLenum
texel_format<rgb_565>() noexcept
{
	return GL_RGB;
}

template <>
constexpr GLenum
texel_type<rgb_565>() noexcept
{
	return GL_UNSIGNED_SHORT_5_6_5;
}

// rgba_8888 -----------------------------------------------------------

template <>
constexpr GLenum
texel_internal_format<rgba_8888>() noexcept
{
	return GL_RGBA8;
}

template <>
constexpr GLenum
texel_format<rgba_8888>() noexcept
{
	return GL_RGBA;
}

template <>
constexpr GLenum
texel_type<rgba_8888>() noexcept
{
	// 0xRR,0xGG,0xBB,0xAA on Intel, little-endian machine is
	// 0xAABBGGRR. GL_UNSIGNED_BYTE preserves the format of binary
	// blocks of data across machines, while GL_UNSIGNED_INT_8_8_8_8
	// preserves the format of literals like 0xRRGGBBAA.
	return GL_UNSIGNED_INT_8_8_8_8;
}

template <typename Colour> class texture {
	GLuint      m_id;
	std::size_t m_width;
	std::size_t m_height;

      public:
	texture(std::size_t width, std::size_t height)
	{
		if (width == 0) {
			throw std::invalid_argument(
				"Cannot create a colour buffer, "
				"because the given width is zero.");
		}

		if (height == 0) {
			throw std::invalid_argument(
				"Cannot create a colour buffer, "
				"because the given height is zero.");
		}

		// TODO: Check GL_MAX_TEXTURE_SIZE
		m_width  = width;
		m_height = height;

		glGenTextures(1, &m_id);
		glBindTexture(GL_TEXTURE_2D, m_id);
		glTexParameteri(
			GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(
			GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			texel_internal_format<Colour>(),
			m_width,
			m_height,
			0,
			texel_format<Colour>(),
			texel_type<Colour>(),
			nullptr);
		check_opengl_error();
	}

	void
	bind()
	{
		glBindTexture(GL_TEXTURE_2D, m_id);
	}

	void
	unbind()
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void
	populate_from_video_surface(video_surface<Colour> &surf)
	{
		surf.bind();
		glTexSubImage2D(
			GL_TEXTURE_2D,
			0,
			0,
			0,
			surf.width(),
			surf.height(),
			texel_format<Colour>(),
			texel_type<Colour>(),
			nullptr);
		check_opengl_error();
		surf.unbind();
	}

	~texture()
	{
		if (m_id != 0) {
			glDeleteTextures(1, &m_id);
			m_id = 0;
		}
	}
};
} // namespace internal

/// Video surface that can render to a screen.
template <typename Colour> class primary_surface {
	// Double buffering
	// static constexpr
	// std::size_t buffers_count = 2;

	// video_surface<Colour>     m_buffers[buffers_count];
	video_surface<Colour>     m_buffer;
	internal::texture<Colour> m_texture;

      public:
	primary_surface(std::size_t width, std::size_t height)
	    : /*m_buffers { video_surface<Colour>{ width, height },
		video_surface<Colour>{ width, height } },*/
	      m_buffer{width, height}, m_texture{width, height}
	{}

	std::size_t
	width() const noexcept
	{
		return m_buffer.width();
		// return m_buffers[0].width();
	}

	std::size_t
	height() const noexcept
	{
		return m_buffer.height();
		// return m_buffers[0].height();
	}

	locked_pixels<Colour>
	lock()
	{
		return m_buffer.lock();
	}

	void
	unlock()
	{
		return m_buffer.unlock();
	}

	void
	swap()
	{
		m_texture.bind();
		m_texture.populate_from_video_surface(m_buffer);
		render();
		m_texture.unbind();
	}

      private:
	void
	render()
	{
		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		{
			glTexCoord2f(0, 0);
			glVertex2f(0, 0);

			// TODO: Calculate this in constructor.
			//                float max = std::max(width(),
			//                height());

			glTexCoord2f(1, 0);
			glVertex2f(width(), 0);

			glTexCoord2f(1, 1);
			glVertex2f(width(), height());

			glTexCoord2f(0, 1);
			glVertex2f(0, height());

			// glTexCoord2f(max / width(), 0);
			// glVertex2f  (width(),       0);

			// glTexCoord2f(max / width(), max / height());
			// glVertex2f  (width(),       height());

			// glTexCoord2f(0, max / height());
			// glVertex2f  (0, height());
		}
		glEnd();
	}
};
} // namespace conker::gfx2d
