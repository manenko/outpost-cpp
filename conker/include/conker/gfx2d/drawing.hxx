//
// conker/include/conker/gfx2d/drawing.hxx
//
// Created by Oleksandr Manenko on 16-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#pragma once

#include <conker/gfx2d/video_surface.hxx>

namespace conker::gfx2d {
template <typename Colour>
inline void
draw_pixel(
	locked_pixels<Colour> &li,
	std::size_t            x,
	std::size_t            y,
	const Colour &         c)
{
	li.ptr[x + y * li.width] = c;
}

template <typename Colour>
void
draw_hline(
	locked_pixels<Colour> &li,
	std::size_t            x,
	std::size_t            y,
	std::size_t            length,
	const Colour &         c)
{
	auto end = x + length;
	for (; x < end; ++x) {
		draw_pixel(li, x, y, c);
	}
}

template <typename Colour>
void
draw_vline(
	locked_pixels<Colour> &li,
	std::size_t            x,
	std::size_t            y,
	std::size_t            length,
	const Colour &         c)
{
	auto end = y + length;
	for (; y < end; ++y) {
		draw_pixel(li, x, y, c);
	}
}

template <typename Colour>
void
draw_rectangle(
	locked_pixels<Colour> &li,
	std::size_t            x,
	std::size_t            y,
	std::size_t            width,
	std::size_t            height,
	const Colour &         c)
{
	draw_hline(li, x, y, width, c);
	draw_vline(li, x, y, height, c);

	draw_hline(li, x, y + height - 1, width, c);
	draw_vline(li, x + width - 1, y, height, c);
}

template <typename Colour>
void
clear(locked_pixels<Colour> &li, const Colour &c)
{
	auto count = li.width * li.height;
	auto ptr   = li.ptr;
	for (std::size_t i = 0; i < count; ++i) {
		*ptr = c;
		++ptr;
	}
}

} // namespace conker::gfx2d
