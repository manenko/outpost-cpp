#pragma once

//
// conker/include/conker/gfx2d/video_surface.hxx
//
// Created by Oleksandr Manenko on 24-06-2018.
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <conker/gfx2d/opengl.hxx>
#include <cstddef>
#include <stdexcept>

namespace conker::gfx2d {
template <typename Colour> class locked_pixels {
      public:
	Colour *const     ptr;
	const std::size_t width;
	const std::size_t height;

	locked_pixels(Colour *ptr, std::size_t width, std::size_t height)
	    : ptr{ptr}, width{width}, height{height}
	{}
};

template <typename Colour> class video_surface {
	GLuint            m_id;
	const std::size_t m_width;
	const std::size_t m_height;
	const std::size_t m_size;

      public:
	video_surface(std::size_t width, std::size_t height)
	    : m_id{0}, m_width{width}, m_height{height}, m_size{width * height *
								sizeof(Colour)}
	{
		if (width == 0) {
			throw std::invalid_argument(
				"Cannot create a video surface, "
				"because the given width is zero.");
		}

		if (height == 0) {
			throw std::invalid_argument(
				"Cannot create a video surface, "
				"because the given height is zero.");
		}

		glGenBuffers(1, &m_id);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_id);
		glBufferData(
			GL_PIXEL_UNPACK_BUFFER,
			m_size,
			nullptr,
			GL_STREAM_DRAW);

		check_opengl_error();

		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	}

	std::size_t
	width() const noexcept
	{
		return m_width;
	}

	std::size_t
	height() const noexcept
	{
		return m_height;
	}

	// TODO: Size in bytes. We need `count` as well
	std::size_t
	size() const noexcept
	{
		return m_size;
	}

	void
	bind() const noexcept
	{
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_id);
	}

	void
	unbind() const noexcept
	{
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	}

	// TODO: Should return a structure with a pointer, size and
	// width.  All drawing functions will use the structure.
	//
	// Maybe we should return a smart ptr, which will ++ on lock
	// and -- on unlock?
	//
	// We should connect clipper here somehow.
	locked_pixels<Colour>
	lock()
	{
		bind();

		glBufferData(
			GL_PIXEL_UNPACK_BUFFER,
			m_size,
			nullptr,
			GL_STREAM_DRAW);
		check_opengl_error();

		auto ptr = static_cast<Colour *>(
			glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_READ_WRITE));
		check_opengl_error();

		return locked_pixels<Colour>{ptr, width(), height()};
	}

	void
	unlock()
	{
		glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
		check_opengl_error();

		unbind();
	}

	~video_surface()
	{
		if (m_id != 0) {
			glDeleteBuffers(1, &m_id);
			m_id = 0;
		}
	}
};
} // namespace conker::gfx2d
