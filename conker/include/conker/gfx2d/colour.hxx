#pragma once

//
// conker/include/conker/gfx2d/colour.hxx
//
// Created by Oleksandr Manenko on 24-06-2018.
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <cstdint>

namespace conker::gfx2d {
enum class colour_format {
	palette_indexed_1,
	palette_indexed_2,
	palette_indexed_4,
	palette_indexed_8,

	xrgb_1555,
	argb_1555,
	xrgb_8888,
	argb_8888,

	rgb_332, //!< Normalized integer, with 3 bits for R
	//!< and G, but only 2 for B.
	rgb_565,
	rgb_888,

	rgbx_5551,
	rgba_5551,
	rgbx_8888,
	rgba_8888,
	rgba_1010102,
	rgba_4444,
};

/// Represents a colour value in `XRGB1555` format.
///
/// A single colour value is 16-bit integer with 5 bits for red,
/// green, and blue. The remaining bit is ignored.
///
/// **Memory representation**
///
/// ```text
///     15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// | X0 | R4 | R3 | R2 | R1 | R0 | G4 | G3 | G2 | G1 | G0 | B4 | B3 | B2 | B1 | B0 |
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
///   ^  |      red component     |     green component    |     blue component
///   |
///   |
/// ignored
/// ```
class xrgb_1555 final {
	std::uint16_t value;

	constexpr std::uint16_t
	truncate(std::uint8_t value) noexcept
	{
		return value & 0x1f;
	}

      public:
	/// Creates a new colour in `XRGB1555` format using the given `red`,
	/// `green`, and `blue` components.
	///
	/// **Important: Value truncation**
	///
	/// The maximum value of a colour component is 0x1f<sub>16</sub>
	/// or 31<sub>10</sub>, because each component occupies 5
	/// bits. For example, black colour (the minimum possible value) in
	/// this format is `R:0, G:0, B:0` and white colour (the
	/// maximum possible value) is `R:31, G:31, B:31`.
	///
	/// The function truncates each colour component to 5 bits. For
	/// example, a value of 0xab<sub>16</sub> (or 171<sub>10</sub>)
	/// becomes 0xb<sub>16</sub> (or 11<sub>10</sub>) and not
	/// 0x1f<sub>16</sub> (or 31<sub>10</sub>).
	constexpr xrgb_1555(
		std::uint8_t red,
		std::uint8_t green,
		std::uint8_t blue) noexcept
	    : value{static_cast<uint16_t>(
		      truncate(blue) + (truncate(green) << 5) +
		      (truncate(red) << 10))}
	{}

	/// Creates a black colour in `XRGB1555` format.
	constexpr xrgb_1555() noexcept : xrgb_1555(0)
	{}

	/// Creates a colour in `XRGB1555` format using the given raw
	/// colour value.
	constexpr xrgb_1555(std::uint16_t value) noexcept : value{value}
	{}

	/// Converts this colour into `std::uint16_t`.
	constexpr operator std::uint16_t() const noexcept
	{
		return value;
	}

	/// Gets red component.
	constexpr std::uint8_t
	red() const noexcept
	{
		return (value & 0x7c00) >> 10;
	}

	/// Gets blue component.
	constexpr std::uint8_t
	green() const noexcept
	{
		return (value & 0x3e0) >> 5;
	}

	/// Gets blue component.
	constexpr std::uint8_t
	blue() const noexcept
	{
		return value & 0x1f;
	}

	/// Changes red component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 5
	/// bits. Refer to the constructor documentation for details.
	constexpr xrgb_1555 &
	set_red(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0x83ff) + (truncate(value) << 10);
		return *this;
	}

	/// Changes green component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 5
	/// bits. Refer to the constructor documentation for details.
	constexpr xrgb_1555 &
	set_green(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xfc1f) + (truncate(value) << 5);
		return *this;
	}

	/// Changes blue component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 5
	/// bits. Refer to the constructor documentation for details.
	constexpr xrgb_1555 &
	set_blue(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xffe0) + truncate(value);
		return *this;
	}
};

/// Represents a colour value in `RGB565` format.
///
/// A single colour value is 16-bit integer with 5 bits for red,
/// 6 for green, and 5 for blue.
///
/// **Memory representation**
///
/// ```text
///     15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// | R4 | R3 | R2 | R1 | R0 | G5 | G4 | G3 | G2 | G1 | G0 | B4 | B3 | B2 | B1 | B0 |
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// |     red component      |       green component       |     blue component
/// |
/// ```
class rgb_565 final {
	std::uint16_t value;

	constexpr std::uint16_t
	truncate5(std::uint8_t value) noexcept
	{
		return value & 0x1f;
	}

	constexpr std::uint16_t
	truncate6(std::uint8_t value) noexcept
	{
		return value & 0x3f;
	}

      public:
	/// Creates a new colour in `RGB565` format using the given `red`,
	/// `green`, and `blue` components.
	///
	/// The maximum value of red and blue in this format is
	/// 0x1f. Green occupies more space (6 bits) and its maximum
	/// value is 0x3f.
	///
	/// **Important!** The function doesn't round values to the
	/// maximum value for the corresponding colour component. It
	/// **truncates** each colour component on a bit level:
	///
	/// - Red   to 5 bits.
	/// - Green to 6 bits.
	/// - Blue  to 5 bits.
	///
	/// For example:
	///
	/// - Red/Blue 0xff (0b1111_1111) becomes 0x1f (0b1_1111)
	/// - Red/Blue 0xab (0b1010_1011) becomes 0xb  (0b0_1011)
	/// - Green    0x4f (0b0100_1111) becomes 0xf  (0b00_1111)
	/// - Green    0xff (0b1111_1111) becomes 0x3f (0b11_1111)
	/// - etc.
	constexpr rgb_565(
		std::uint8_t red,
		std::uint8_t green,
		std::uint8_t blue) noexcept
	    : value{static_cast<std::uint16_t>(
		      truncate5(blue) + (truncate6(green) << 5) +
		      (truncate5(red) << 11))}
	{}

	/// Creates black colour.
	constexpr rgb_565() noexcept : rgb_565(0)
	{}

	/// Creates a colour in `RGB565` format using the given raw
	/// colour `value`.
	constexpr rgb_565(std::uint16_t value) noexcept : value{value}
	{}

	/// Converts this colour into `std::uint16_t`.
	constexpr operator std::uint16_t() const noexcept
	{
		return value;
	}

	/// Gets red component.
	constexpr std::uint8_t
	red() const noexcept
	{
		return (value & 0xf800) >> 11;
	}

	/// Gets green component.
	constexpr std::uint8_t
	green() const noexcept
	{
		return (value & 0x7e0) >> 5;
	}

	/// Gets blue component.
	constexpr std::uint8_t
	blue() const noexcept
	{
		return value & 0x1f;
	}

	/// Changes red component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 5
	/// bits. Refer to the constructor documentation for details.
	constexpr rgb_565 &
	set_red(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0x07ff) + (truncate5(value) << 11);
		return *this;
	}

	/// Changes green component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 6
	/// bits. Refer to the constructor documentation for details.
	constexpr rgb_565 &
	set_green(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xf81f) + (truncate6(value) << 5);
		return *this;
	}

	/// Changes blue component to the given `value`.
	///
	/// **Note:** The function truncates the `value` to 5
	/// bits. Refer to the constructor documentation for details.
	constexpr rgb_565 &
	set_blue(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xffe0) + truncate5(value);
		return *this;
	}
};

/// Represents a colour value in `RGBX8888` format.
///
/// A single colour value is 32-bit integer with 8 bits for red,
/// green, and blue.
///
/// **Memory representation**
///
/// ```text
/// RRRRRRRR GGGGGGGG BBBBBBBB XXXXXXXX
/// ```
class rgbx_8888 final {
	std::uint32_t value;

	constexpr uint32_t
	make_red(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 24;
	}

	constexpr uint32_t
	make_green(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 16;
	}

	constexpr uint32_t
	make_blue(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 8;
	}

	constexpr uint32_t
	from(std::uint8_t red, std::uint8_t green, std::uint8_t blue) const
		noexcept
	{
		return make_red(red) + make_green(green) + make_blue(blue);
	}

      public:
	/// Creates a new colour in `rgbx_8888` format using the given `red`,
	/// `green`, and `blue` components.
	constexpr rgbx_8888(
		std::uint8_t red,
		std::uint8_t green,
		std::uint8_t blue) noexcept
	    : value{from(red, green, blue)}
	{}

	/// Creates black colour.
	constexpr rgbx_8888() noexcept : rgbx_8888(0, 0, 0)
	{}

	/// Creates a colour in `rgbx_8888` format using the given raw
	/// colour `value`.
	constexpr rgbx_8888(std::uint32_t value) noexcept
	    : value{value & 0xffffff00}
	{}

	/// Converts this colour into `std::uint32_t`.
	constexpr operator std::uint32_t() const noexcept
	{
		return value;
	}

	/// Gets red component.
	constexpr std::uint8_t
	red() const noexcept
	{
		return (value & 0xff000000) >> 24;
	}

	/// Gets green component.
	constexpr std::uint8_t
	green() const noexcept
	{
		return (value & 0x00ff0000) >> 16;
	}

	/// Gets blue component.
	constexpr std::uint8_t
	blue() const noexcept
	{
		return (value & 0x0000ff00) >> 8;
	}

	/// Changes red component to the given `value`.
	constexpr rgbx_8888 &
	set_red(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0x00ffff00) + make_red(value);
		return *this;
	}

	/// Changes green component to the given `value`.
	constexpr rgbx_8888 &
	set_green(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xff00ff00) + make_green(value);
		return *this;
	}

	/// Changes blue component to the given `value`.
	constexpr rgbx_8888 &
	set_blue(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xffff0000) + make_blue(value);
		return *this;
	}
};

/// Represents a colour value in `RGBA8888` format.
///
/// A single colour value is 32-bit integer with 8 bits for red,
/// green, blue, and alpha.
///
/// **Memory representation**
///
/// ```text
/// RRRRRRRR GGGGGGGG BBBBBBBB AAAAAAAA
/// ```
class rgba_8888 final {
	std::uint32_t value;

	constexpr uint32_t
	make_red(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 24;
	}

	constexpr uint32_t
	make_green(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 16;
	}

	constexpr uint32_t
	make_blue(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value) << 8;
	}

	constexpr uint32_t
	make_alpha(uint8_t value) const noexcept
	{
		return static_cast<uint32_t>(value);
	}

	constexpr uint32_t
	from(std::uint8_t red,
	     std::uint8_t green,
	     std::uint8_t blue,
	     std::uint8_t alpha) const noexcept
	{
		return make_red(red) + make_green(green) + make_blue(blue) +
		       make_alpha(alpha);
	}

      public:
	/// Creates a new colour in `rgba_8888` format using the given `red`,
	/// `green`, `blue`, and `alpha` components.
	constexpr rgba_8888(
		std::uint8_t red,
		std::uint8_t green,
		std::uint8_t blue,
		std::uint8_t alpha) noexcept
	    : value{from(red, green, blue, alpha)}
	{}

	/// Creates black colour.
	constexpr rgba_8888() noexcept : rgba_8888(0, 0, 0, 0xff)
	{}

	/// Creates a colour in `rgba_8888` format using the given raw
	/// colour `value`.
	constexpr rgba_8888(std::uint32_t value) noexcept : value{value}
	{}

	/// Converts this colour into `std::uint32_t`.
	constexpr operator std::uint32_t() const noexcept
	{
		return value;
	}

	/// Gets red component.
	constexpr std::uint8_t
	red() const noexcept
	{
		return (value & 0xff000000) >> 24;
	}

	/// Gets green component.
	constexpr std::uint8_t
	green() const noexcept
	{
		return (value & 0x00ff0000) >> 16;
	}

	/// Gets blue component.
	constexpr std::uint8_t
	blue() const noexcept
	{
		return (value & 0x0000ff00) >> 8;
	}

	/// Gets alpha component.
	constexpr std::uint8_t
	alpha() const noexcept
	{
		return value & 0x000000ff;
	}

	/// Changes red component to the given `value`.
	constexpr rgba_8888 &
	set_red(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0x00ffffff) + make_red(value);
		return *this;
	}

	/// Changes green component to the given `value`.
	constexpr rgba_8888 &
	set_green(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xff00ffff) + make_green(value);
		return *this;
	}

	/// Changes blue component to the given `value`.
	constexpr rgba_8888 &
	set_blue(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xffff00ff) + make_blue(value);
		return *this;
	}

	/// Changes alpha component to the given `value`.
	constexpr rgba_8888 &
	set_alpha(std::uint8_t value) noexcept
	{
		this->value = (this->value & 0xffffff00) + make_alpha(value);
		return *this;
	}
};
} // namespace conker::gfx2d
