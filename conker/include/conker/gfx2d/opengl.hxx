//
// conker/include/conker/gfx2d/opengl.hxx
//
// Created by Oleksandr Manenko on 15-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#pragma once

#include <conker/gfx2d/opengl/glad/glad.h>
#include <cstddef>
#include <stdexcept>
#include <string>

namespace conker::gfx2d {
class opengl_error : public std::runtime_error {
	GLenum m_code;

      public:
	opengl_error(const std::string &what_arg, GLenum code)
	    : std::runtime_error(what_arg), m_code{code}
	{}

	explicit opengl_error(GLenum code)
	    : opengl_error("OpenGL operation has failed", code)
	{}

	GLenum
	code() const noexcept
	{
		return m_code;
	}
};

void
check_opengl_error(GLenum code)
{
	if (code != GL_NO_ERROR) {
		throw opengl_error(code);
	}
}

void
check_opengl_error()
{
	check_opengl_error(glGetError());
}

void
setup_2d_camera(std::size_t framebuffer_width, std::size_t framebuffer_height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, framebuffer_width, framebuffer_height);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0, framebuffer_width, framebuffer_height, 0, 1, -1);
}
} // namespace conker::gfx2d

