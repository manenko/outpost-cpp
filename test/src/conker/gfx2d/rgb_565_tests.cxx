//
// test/src/conker/gfx2d/rgb_565_tests.cxx
//
// Created by Oleksandr Manenko on 07-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <catch.hpp>
#include <conker/gfx2d/colour.hxx>

using conker::gfx2d::rgb_565;

TEST_CASE("rgb_565 initialized to 0 by default", "[conker-gfx2d]")
{
	REQUIRE(rgb_565() == 0);
}

TEST_CASE(
	"rgb_565 initialized by a single std::uint16_t value",
	"[conker-gfx2d]")
{
	REQUIRE(rgb_565(0xabc) == 0xabc);
}

TEST_CASE("rgb_565 initialized by r,g,b triage", "[conker-gfx2d]")
{
	REQUIRE(rgb_565(0x1f, 0x3f, 0x1f) == 0xffff);
	REQUIRE(rgb_565(0x00, 0x3f, 0x00) == 0x07e0);
}

TEST_CASE("rgb_565 returns correct value for red", "[conker-gfx2d]")
{
	REQUIRE(rgb_565(0xff, 0x1f, 0x0f).red() == 0x1f);
	REQUIRE(rgb_565(0x00, 0xff, 0xff).red() == 0x00);
	REQUIRE(rgb_565(0x13, 0x12, 0x0c).red() == 0x13);
}

TEST_CASE("rgb_565 returns correct value for green", "[conker-gfx2d]")
{
	REQUIRE(rgb_565(0xaf, 0x3f, 0xcf).green() == 0x3f);
	REQUIRE(rgb_565(0x1f, 0x5f, 0xff).green() == 0x1f);
	REQUIRE(rgb_565(0x1f, 0x6f, 0x00).green() == 0x2f);
}

TEST_CASE("rgb_565 returns correct value for blue", "[conker-gfx2d]")
{
	REQUIRE(rgb_565(0xaf, 0x01, 0x16).blue() == 0x16);
	REQUIRE(rgb_565(0x1f, 0xff, 0xff).blue() == 0x1f);
	REQUIRE(rgb_565(0x1f, 0x04, 0x00).blue() == 0x00);
}

TEST_CASE("rgb_565 correctly changes red", "[conker-gfx2d]")
{
	auto colour = rgb_565(0xaf, 0xff, 0xab);
	REQUIRE(colour == 0x7feb);

	colour.set_red(0x04);

	REQUIRE(colour.red() == 0x04);
	REQUIRE(colour == 0x27eb);
}

TEST_CASE("rgb_565 correctly changes green", "[conker-gfx2d]")
{
	auto colour = rgb_565(0x1f, 0xff, 0xaa);
	REQUIRE(colour == 0xffea);

	colour.set_green(0xac);

	REQUIRE(colour.green() == 0x2c);
	REQUIRE(colour == 0xfd8a);
}

TEST_CASE("rgb_565 correctly changes blue", "[conker-gfx2d]")
{
	auto colour = rgb_565(0x1f, 0xff, 0xa7);
	REQUIRE(colour == 0xffe7);

	colour.set_blue(0xf7);

	REQUIRE(colour.blue() == 0x17);
	REQUIRE(colour == 0xfff7);
}
