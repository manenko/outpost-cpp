//
// test/src/conker/gfx2d/xrgb_1555_tests.cxx
//
// Created by Oleksandr Manenko on 04-07-2018.
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <catch.hpp>
#include <conker/gfx2d/colour.hxx>

using conker::gfx2d::xrgb_1555;

TEST_CASE("xrgb_1555 initialized to 0 by default", "[conker-gfx2d]")
{
	REQUIRE(xrgb_1555() == 0);
}

TEST_CASE(
	"xrgb_1555 initialized by a single std::uint16_t value",
	"[conker-gfx2d]")
{
	REQUIRE(xrgb_1555(0xabc) == 0xabc);
}

TEST_CASE("xrgb_1555 initialized by r,g,b triage", "[conker-gfx2d]")
{
	REQUIRE(xrgb_1555(0xff, 0xff, 0xff) == 0x7fff);
	REQUIRE(xrgb_1555(0x00, 0x1f, 0x00) == 0x03e0);
	REQUIRE(xrgb_1555(0x00, 0x00, 0x1f) == 0x001f);
	REQUIRE(xrgb_1555(0x1f, 0x00, 0x00) == 0x7c00);
	REQUIRE(xrgb_1555(0xff, 0x00, 0x00) == 0x7c00);
	REQUIRE(xrgb_1555(0x00, 0x00, 0x00) == 0x0000);
	REQUIRE(xrgb_1555(0xaf, 0xff, 0xab) == 0x3feb);
	REQUIRE(xrgb_1555(0x1f, 0xff, 0xaa) == 0x7fea);
	REQUIRE(xrgb_1555(0x1f, 0xff, 0xa7) == 0x7fe7);
}

TEST_CASE("xrgb_1555 returns correct value for red", "[conker-gfx2d]")
{
	REQUIRE(xrgb_1555(0xff, 0x1f, 0x0f).red() == 0x1f);
	REQUIRE(xrgb_1555(0x00, 0xff, 0xff).red() == 0x00);
	REQUIRE(xrgb_1555(0x13, 0x12, 0x0c).red() == 0x13);
}

TEST_CASE("xrgb_1555 returns correct value for green", "[conker-gfx2d]")
{
	REQUIRE(xrgb_1555(0xaf, 0x01, 0xcf).green() == 0x01);
	REQUIRE(xrgb_1555(0x1f, 0xff, 0xff).green() == 0x1f);
	REQUIRE(xrgb_1555(0x1f, 0x04, 0x00).green() == 0x04);
}

TEST_CASE("xrgb_1555 returns correct value for blue", "[conker-gfx2d]")
{
	REQUIRE(xrgb_1555(0xaf, 0x01, 0x16).blue() == 0x16);
	REQUIRE(xrgb_1555(0x1f, 0xff, 0xff).blue() == 0x1f);
	REQUIRE(xrgb_1555(0x1f, 0x04, 0x00).blue() == 0x00);
}

TEST_CASE("xrgb_1555 correctly changes red", "[conker-gfx2d]")
{
	auto colour = xrgb_1555(0xaf, 0xff, 0xab);
	REQUIRE(colour == 0x3feb);

	colour.set_red(0x04);

	REQUIRE(colour.red() == 0x04);
	REQUIRE(colour == 0x13eb);
}

TEST_CASE("xrgb_1555 correctly changes green", "[conker-gfx2d]")
{
	auto colour = xrgb_1555(0x1f, 0xff, 0xaa);
	REQUIRE(colour == 0x7fea);

	colour.set_green(0xac);

	REQUIRE(colour.green() == 0x0c);
	REQUIRE(colour == 0x7d8a);
}

TEST_CASE("xrgb_1555 correctly changes blue", "[conker-gfx2d]")
{
	auto colour = xrgb_1555(0x1f, 0xff, 0xa7);
	REQUIRE(colour == 0x7fe7);

	colour.set_blue(0xf7);

	REQUIRE(colour.blue() == 0x17);
	REQUIRE(colour == 0x7ff7);
}
