//
// test/src/conker/gfx2d/rgbx_8888_tests.cxx
//
// Created by Oleksandr Manenko on 08-07-2018
// Copyright © 2018 Oleksandr Manenko. All rights reserved.
//

#include <catch.hpp>
#include <conker/gfx2d/colour.hxx>

using conker::gfx2d::rgbx_8888;

TEST_CASE("rgbx_8888 initialized to 0 by default", "[conker-gfx2d]")
{
	REQUIRE(rgbx_8888() == 0);
}

TEST_CASE(
	"rgbx_8888 initialized by a single std::uint32_t value",
	"[conker-gfx2d]")
{
	REQUIRE(rgbx_8888(0xabcdefff) == 0xabcdef00);
}

TEST_CASE("rgbx_8888 initialized by r,g,b triage", "[conker-gfx2d]")
{
	REQUIRE(rgbx_8888(0xff, 0xff, 0xff) == 0xffffff00);
	REQUIRE(rgbx_8888(0xff, 0x00, 0x00) == 0xff000000);
	REQUIRE(rgbx_8888(0x00, 0xff, 0x00) == 0x00ff0000);
	REQUIRE(rgbx_8888(0x00, 0x00, 0xff) == 0x0000ff00);
}

TEST_CASE("rgbx_8888 returns correct value for red", "[conker-gfx2d]")
{
	REQUIRE(rgbx_8888(0xaa, 0x1f, 0x0f).red() == 0xaa);
	REQUIRE(rgbx_8888(0x00, 0xbb, 0xff).red() == 0x00);
	REQUIRE(rgbx_8888(0x13, 0x12, 0x0c).red() == 0x13);
}

TEST_CASE("rgbx_8888 returns correct value for green", "[conker-gfx2d]")
{
	REQUIRE(rgbx_8888(0xaf, 0x01, 0xcf).green() == 0x01);
	REQUIRE(rgbx_8888(0x1f, 0xff, 0xff).green() == 0xff);
	REQUIRE(rgbx_8888(0x1f, 0x04, 0x00).green() == 0x04);
}

TEST_CASE("rgbx_8888 returns correct value for blue", "[conker-gfx2d]")
{
	REQUIRE(rgbx_8888(0xaf, 0x01, 0x16).blue() == 0x16);
	REQUIRE(rgbx_8888(0x1f, 0xff, 0xff).blue() == 0xff);
	REQUIRE(rgbx_8888(0x1f, 0x04, 0x00).blue() == 0x00);
}

TEST_CASE("rgbx_8888 correctly changes red", "[conker-gfx2d]")
{
	auto colour = rgbx_8888(0xaf, 0xff, 0xab);
	REQUIRE(colour == 0xafffab00);

	colour.set_red(0x04);

	REQUIRE(colour.red() == 0x04);
	REQUIRE(colour == 0x04ffab00);
}

TEST_CASE("rgbx_8888 correctly changes green", "[conker-gfx2d]")
{
	auto colour = rgbx_8888(0x1f, 0xff, 0xaa);
	REQUIRE(colour == 0x1fffaa00);

	colour.set_green(0xac);

	REQUIRE(colour.green() == 0xac);
	REQUIRE(colour == 0x1facaa00);
}

TEST_CASE("rgbx_8888 correctly changes blue", "[conker-gfx2d]")
{
	auto colour = rgbx_8888(0x1f, 0xff, 0xa7);
	REQUIRE(colour == 0x1fffa700);

	colour.set_blue(0xf7);

	REQUIRE(colour.blue() == 0xf7);
	REQUIRE(colour == 0x1ffff700);
}
